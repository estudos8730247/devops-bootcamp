FROM httpd:latest
WORKDIR /usr/local/apache2/htdocs/
RUN rm -R /usr/local/apache2/htdocs/*
COPY ./application/ /usr/local/apache2/htdocs/ 

EXPOSE 80