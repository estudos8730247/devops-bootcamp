<h1 align="center"> Pipeline Bootcamp DevOps</h1>
</br>
<p>
Pipeline criada afim de automatizar o deploy de um site e todos os processos por trás, como:</br> criação de imagens e contêiners docker,</br> deploy, verificação e exclusão de contêiners que já existam com o mesmo nome.</br>
<strong align="center">Por que fazer manual se podemos automatizar?</strong>
</br>

![wehavetechnology](https://github.com/Giovanna-Azzolini/TelaLogin-DarkLigth/assets/122488274/cf1b263e-d294-440b-9849-622572feefe8)
</p>
</br>

<p>Da uma olhadinha nessa aplicação que fiz com tanto carinho - https://lnkd.in/dZDu5jGU </p>

</br> 

## DESENVOLVIMENTO:

- Comece criando uma pasta e incluindo uma aplicação que você tenha desenvolvido (index.html)
- Crie um repositório no gitlab e faça um commit bonitão (após criar, o próprio gitlab te dá os comandos)
- Abra essa pasta no vscode ou na IDE que você costuma utilizar e crie uma Dockerfile com a imagem (no meu caso foi um apache), diretório e a porta que sua aplicação ficará alocada (usa a 80 que é padrão sucesso)

## OPERAÇÕES (Utilizando servidor Linux/Ubuntu)

- Agora vamos instalar nosso querido Docker vulgo baleia azul (pesquisa que o google te dá os comandos na mão)
- Vamos instalar o Gitlab-Runner? Segue isso ai que é tiro certeiro 
    - curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
    - sudo apt-get install gitlab-runner
- Não esquece de registrar e iniciar o GItlab-Runner 
- Você também precisará  dar permissões para que o usuário gitlab-runner use o docker
- Por fim logue na sua conta do Docker 

## DevOps:

    - Crie um arquivo chamado .gitlab-ci.yml  para criar sua pipeline, definindo os stages e os processos que serão feitos

</br>

<h1 align="center">Desafio feito no <strong>Bootcamp de DevOps</strong> da empresa  Atlantico e ministrado pelo incrivel Professor <strong>Denilson Bonatti</strong></h1>




